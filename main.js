$(document).ready(function() {
    // ====== TABS START =========

    var tabsName = $('.tabs-head a');

    tabsName.on('click', function(e){

        e.preventDefault();
        var that = $(this);

        that.parent().addClass('active');
        that.parent()
            .siblings()
            .removeClass('active');

        var tab = that.attr('href');
        var tabsContent = $('.tabs-content li');

        tabsContent.not('tab').css('display', 'none');
        $(tab).fadeIn();
    });

    // ====== TABS END =========

    // ====== COLLAPSE START =========

    var collapse = $('.my-collapse h4');
    var collaspceContent = $('.my-collapse-content');

    collapse.on('click', function(e){

        e.preventDefault();
        var that = $(this);

        if(that.hasClass('active')){
            that.removeClass('active')
        }else{
            that.addClass('active');
        }

        that.siblings().removeClass('active');
        that.next().stop(true, true).slideToggle();
        collaspceContent.not(that.next()).stop(true, true).slideUp();

    });

    // ====== COLLAPSE END =========

    // ====== GALLERY START ==========

    $('.gallery-thumbs').find('img').on('click', function(e){

        e.preventDefault();
        var that = $(this);
        var galleryDisplay = $('.gallery-disp').find('img');
        var galleryThumbsItem = that.closest('li');
        var thumbSrc =  that.attr('src');
        var duration = 200;

        if(!galleryThumbsItem.hasClass('active')){

            galleryThumbsItem
                            .addClass('active')
                            .siblings()
                            .removeClass('active');

            galleryDisplay.fadeOut(duration, function(){
                $(this)
                    .attr('src', thumbSrc)
                    .fadeIn(duration);
            });
        }
    });

    // ====== GALLERY END ==========

    // ====== GALLERY START ==========

    $('.slider-btn').on('click', function(){

        var that = $(this);

        var container = that.closest('.slider-wrap');
        var items = container.find('.slider-item');
        var activeItem = items.filter('.active');

        if($(this).hasClass('next')){
            var hasNext = activeItem.next().length;
            setImage((hasNext) ? activeItem.next() : items.first())
        }

        if($(this).hasClass('prev')){
            var hasPrev = activeItem.prev().length;
            setImage((hasPrev) ? activeItem.prev() : items.last());
        }

        function setImage(img) {
            updatePosition(img);
            addAndRemoveActClass(img);
        }

        function updatePosition(slide) {
            container.find('.slider-list').css('left', '-=' + findPos(slide) + 'px');
        }


        function addAndRemoveActClass(thisSlide){
            thisSlide.addClass('active').siblings().removeClass('active');
        }

        function findPos(slide){
            return slide.offset().left - container.offset().left;
        }

    });

    // ====== GALLERY END ==========
    // ====== MODAL START==========

    var myModal = $('.somemodal');
    var modalClick = $('.modal-click');
    var modalClose = $('.close');

    function Modal(modal, close){
        this.modal  = modal;
        this.close = close;
        this.duration = 300;
        this.showOnClick();
        this.hideOnClick();
    }

    Modal.prototype._show = function(modal){
        this.modal.filter(modal)
            .fadeIn(this.duration)
            .css('display', 'block');
    };

    Modal.prototype._hide = function(modal){
        this.modal.filter(modal)
            .fadeOut(this.duration)
            .css('display', 'node');
    };


    Modal.prototype.showOnClick = function(){
        var self = this;
        $(document).on('click', '[data-modal]', function(e){
            var $this   = $(this);
            var $target = $('.' +$this.attr('data-modal'));
            console.log($target);
            e.preventDefault();
            self._show($target);
        });
    };
    Modal.prototype.hideOnClick = function(){
        var self = this;
        this.close.on('click', function(e){
            e.preventDefault();
            self._hide(self.modal);
        });
    };

    new Modal(myModal, modalClose);

    var anothermodal = $('.anothermodal');

    new Modal(anothermodal, modalClose);

    // ====== MODAL END ==========

}); // doc ready end

