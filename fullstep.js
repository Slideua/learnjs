// ====== SQUARES ==========

$(document).ready(function(){

    var squaresWrapper = $('.squares');
    var squaresCount =  squaresWrapper.children().length-1;

    function getRandomPos(min, max) {
        return parseInt(Math.random() * (max - min) + min);
    }

    var randomPosIndex = getRandomPos(1, squaresCount);
    var activeSquare = $('.square.active');
    var randElemPos = squaresWrapper.children().eq(randomPosIndex).position();

//set to active square random position
    activeSquare.css({
        'left' : randElemPos.left,
        'top' : randElemPos.top
    });

    $('body').on('keydown', function(e){

        e.preventDefault();
        var activeSquareWidth = activeSquare.outerWidth();

        switch(e.keyCode){
            //left arrow
            case 37:
                while(parseInt(activeSquare.css('left')) > 0){
                    activeSquare.css({
                        left: '-=' + activeSquareWidth
                    });
                }
                break;
            //top arrow
            case 38:
                while(parseInt(activeSquare.css('top')) > 0){
                    activeSquare.css({
                        top: '-=' + activeSquareWidth
                    });
                }
                break;
            //right arrow
            case 39:
                var wrapperWidth = $('.squares').outerWidth();
                while(parseInt(activeSquare.css('left')) <= (wrapperWidth - activeSquareWidth*2)){
                    activeSquare.css({
                        left: '+=' + activeSquareWidth
                    });
                }
                break;
            //bottom arrow
            case 40:
                var wrapperHeight = $('.squares').outerHeight();
                var activeSquareHeight = activeSquare.outerHeight();
                while(parseInt(activeSquare.css('top')) <= (wrapperHeight - activeSquareHeight*2)){
                    activeSquare.css({
                        top: '+=' + activeSquareHeight
                    });
                }
                break;
        }

    });

}); //doc ready end
